/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "ResourceApplication.h"

#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/loader/JsonLoader.h>
#include <nox/app/resource/data/JsonExtraData.h>

#include <json/value.h>
#include <cassert>

ResourceApplication::ResourceApplication():
	SdlApplication("04-resources", "SuttungDigital")
{
}

bool ResourceApplication::initializeResourceCache()
{
	// We create an LRU cacje with a 512 MiB large cache.
	const auto cacheSizeMb = 512u;
	auto resourceCache = std::make_unique<nox::app::resource::LruCache>(cacheSizeMb);

	// We want the cache to be able to log, so that we can see any errors.
	resourceCache->setLogger(this->createLogger());

	/*
	 * Add a provider that can provide the cache with resources from the filesystem.
	 * It is initialized to assetDirectory so that when you ask the resource cache
	 * about a resource, the path is relative to the assetDirectory.
	 *
	 * The path we pass here uses normal filesystem lookup rules, so it will look in the current
	 * working directory for "assets". Thus it is very important that the working directory is set
	 * properly.
	 *
	 * The initialization of the provider can fail, in that case we return false to abort.
	 */
	const auto assetDirectory = std::string{"assets/"};
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(assetDirectory)) == false)
	{
		return false;
	}

	/*
	 * Add a loader that can load JSON files.
	 * The JsonLoader has a wildcard math "*.json" so that any files ending with
	 * ".json" will be loaded by this loader.
	 * The loader is initialized with a logger, so that it can log any errors.
	 *
	 * The resource cache also always has a DefaultLoader that loads any resources not matching any
	 * other loaders (for this example it will be all non-json files). This loader loads resources
	 * as raw buffers.
	 *
	 * You will see both loaders being used in onInit().
	 */
	resourceCache->addLoader(std::make_unique<nox::app::resource::JsonLoader>(this->createLogger()));

	/*
	 * We move the resource cache over to the the application (the inherited base class Application) so that it can manage it
	 * automatically, and other subsystems can access it. The resource cache can be accessed through the
	 * nox::app::IApplicationContext::getResourceAccess() interface. The resource cache will be automatically destroyed when the application shuts down.
	 */
	this->setResourceCache(std::move(resourceCache));

	return true;
}

bool ResourceApplication::onInit()
{
	if (SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("ResourceApplication");

	/*
	 * We initialize the resource cache, and if it fails we return false to abort.
	 */
	if (this->initializeResourceCache() == false)
	{
		this->log.error().raw("Failed to initialize resource cache.");
		return false;
	}

	/*
	 * We get access to resources through the nox::app::IApplicationContext::getResourceAccess() interface (this class
	 * inherits from nox::app::IApplicationContext through SdlApplciation and Application).
	 *
	 * The LruCache that were created in initializeResourceCache implements the nox::app::resource::IResourceAccess
	 * that is returned from getResourceAccess(). So we basically get a pointer back to the LruCache that we moved
	 * to the Application with setResourceCache().
	 *
	 * For this variable and some other below I'm not using auto. This is so that you can more easily see what type we
	 * are operating on. I would normally use auto.
	 */
	nox::app::resource::IResourceAccess* resourceAccess = this->getResourceAccess();

	/*
	 * Through the resource access interface we request a handle to "rawFile.txt"
	 */
	const auto rawFileDescriptor = nox::app::resource::Descriptor{"rawFile.txt"};
	std::shared_ptr<nox::app::resource::Handle> rawResource = resourceAccess->getHandle(rawFileDescriptor);

	/*
	 * The handle returned may be a nullptr if the resource wasn't found, so we have to check for this.
	 */
	if (rawResource != nullptr)
	{
		/*
		 * We create a std::string object from the buffer of the raw resource using the buffer pointer
		 * and the size of the buffer.
		 */
		auto rawString = std::string{rawResource->getResourceBuffer(), rawResource->getResourceBufferSize()};

		/*
		 * Output to the console so that we can see what was parsed.
		 */
		this->log.info().format("Parsed string from \"%s\": \"%s\"", rawFileDescriptor.getPath().c_str(), rawString.c_str());
	}
	else
	{
		this->log.error().format("Could not load resource: %s", rawFileDescriptor.getPath().c_str());
	}

	/*
	 * Do the same as above, only for a JSON file.
	 */
	nox::app::resource::Descriptor jsonFileDescriptor = "jsonFile.json";
	auto jsonResource = resourceAccess->getHandle(jsonFileDescriptor);

	if (jsonResource != nullptr)
	{
		/*
		 * For JSON files the JsonLoader that we created in initializeResourceCache() is used.
		 * This loader loads the JSON data into a Json::Value object using the jsoncpp library.
		 * This value is stored in the nox::app::resource::JsonExtraData class which can be accessed
		 * with getExtraData<nox::app::resource::JsonExtraData>().
		 */
		auto jsonExtra = jsonResource->getExtraData<nox::app::resource::JsonExtraData>();

		// If jsonExtra is nullptr, something is really wrong.
		assert(jsonExtra != nullptr);

		// We get the root JSON value from the file.
		const Json::Value& fileJson = jsonExtra->getRootValue();

		// We get the JSON properties from the file.
		const auto question = fileJson.get("question", "").asString();
		const auto answer = fileJson.get("answer", 0).asInt();

		// Finally we output the data we got.
		this->log.info().format("Question: %s", question.c_str());
		this->log.info().format("Answer: %d", answer);
	}
	else
	{
		this->log.error().format("Could not load resource: %s", jsonFileDescriptor.getPath().c_str());
	}

	return true;
}
