#ifndef CIRCULARMOVEMENT_H_
#define CIRCULARMOVEMENT_H_

#include <nox/nox.h>
#include <glm/glm.hpp>


class CircularMovement: public nox::logic::actor::Component
{
public:
	static const Component::IdType NAME;

	CircularMovement();

	void onUpdate(const nox::Duration& deltaTime);

	void initialize(const Json::Value& json) override;
	void serialize(Json::Value& json) override;

	const Component::IdType& getName() const override;

private:
	nox::Duration elapsedTime;

	bool hasOriginalPosition;
	glm::vec2 originalPosition;
};


#endif
