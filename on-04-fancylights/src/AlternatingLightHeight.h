#ifndef ALTERNATINGLIGHTHEIGHT_H_
#define ALTERNATINGLIGHTHEIGHT_H_

#include <nox/nox.h>


class AlternatingLightHeight: public nox::logic::actor::Component
{
public:
	static const Component::IdType NAME;

	void onUpdate(const nox::Duration& deltaTime);

	void initialize(const Json::Value& json) override;
	void serialize(Json::Value& json) override;

	const Component::IdType& getName() const override;

private:
	nox::Duration elapsedTime;
};


#endif
