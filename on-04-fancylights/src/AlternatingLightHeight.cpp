#include "AlternatingLightHeight.h"


const nox::logic::actor::Component::IdType AlternatingLightHeight::NAME = "AlternatingLightHeight";

void AlternatingLightHeight::onUpdate(const nox::Duration& deltaTime)
{
	elapsedTime += deltaTime;

	const unsigned ms = std::chrono::duration_cast<std::chrono::milliseconds>(elapsedTime).count();
	const float seconds = (float)ms / 1000.f;
	const float height = 2.1 + (std::cos(seconds * 2) * 2);

	auto lightComponent = this->getOwner()->findComponent<nox::logic::graphics::ActorLight>();
	if (lightComponent != nullptr)
	{
		const int count = lightComponent->getNumberOfLights();
		for (int i=0; i<count; i++)
		{
			nox::app::graphics::Light* light = lightComponent->getLightByIndex(i);
			light->setZHeight(height);
		}
	}
}

void AlternatingLightHeight::initialize(const Json::Value&)
{
}

void AlternatingLightHeight::serialize(Json::Value& json)
{
}

const nox::logic::actor::Component::IdType& AlternatingLightHeight::getName() const
{
	return NAME;
}
