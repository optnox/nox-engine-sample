/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "NetworkApplication.h"

#include <json/value.h>
#include <cassert>
#include <random>


NetworkApplication::NetworkApplication():
	nox::app::SdlApplication("08-event", "SuttungDigital"),
	eventListener(this->getName()),
	eventBroadcaster(nullptr),
    isClient(false),
    isServer(false),
    cListenToBroadcast(false),
    cConnectToBroadcast(false),
    cConnect(false),
    cIsConnected(false),
    cNetMgr(nullptr),
    sBroadcast(false),
    sAutoBan(false),
    sBanDuration(0),
    sNetMgr(nullptr)
{
}

nox::logic::Logic* NetworkApplication::initializeLogic()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	auto logicPtr = logic.get();
	logicPtr->pause(false);

	this->addProcess(std::move(logic));

	return logicPtr;
}

bool NetworkApplication::onInit()
{
	if (SdlApplication::onInit() == false)
	{
		return false;
	}


	this->log = this->createLogger();
	this->log.setName("NetworkApplication");

	this->logic = this->initializeLogic();

	this->eventBroadcaster = this->logic->getEventBroadcaster();

    this->parseCmdArgs();

	this->eventListener.setup(this, this->eventBroadcaster);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ClientConnected::ID);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ClientDisconnected::ID);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ConnectionStarted::ID);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ConnectionFailed::ID);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ConnectionSuccess::ID);

    if (this->isClient)
    {
        this->initClient();
    }

    if (this->isServer)
    {
        this->initServer();
    }

    this->eventListener.startListening();

	return true;
}

void NetworkApplication::initServer()
{
    auto snv = std::make_unique<nox::logic::net::ServerNetworkManager>(
            &this->packetTranslator, &this->serverDelegate);
    this->sNetMgr = snv.get();

    this->logic->setServerNetworkManager(std::move(snv));

    // Start a server on a potentially free port (this is required)
    if (!this->sNetMgr->startServer(8989))
    {
        this->log.fatal().raw("Unable to start server on port 8989");
    }

    if (this->sBroadcast)
    {
        // Enable broadcasting of our location
        this->sNetMgr->setEnableDiscoveryBroadcast(true);
    }
}

void NetworkApplication::initClient()
{
    if (this->cUserData.getUserName() == "")
    {
        this->cUserData.setUserName("Unnamed Client");
    }

	// Setup our ListenerManager to manage this listening to this->eventBroadcaster.
	this->eventListener.addEventTypeToListenFor(nox::logic::net::ServerBroadcastEvent::ID);
	this->eventListener.startListening();

    auto cnv = std::make_unique<nox::logic::net::ClientNetworkManager>(&this->packetTranslator);
    this->cNetMgr  = cnv.get();

    if (this->cListenToBroadcast)
    {
        this->cNetMgr->setBroadcastListening(true);
    }

    this->logic->setClientNetworkManager(std::move(cnv));

    if (this->cConnect)
    {
        connectToServer(this->cHostname, SERV_TCP_PORT);
    }
}

void NetworkApplication::connectToServer(std::string host, Uint16 port)
{
    this->log.debug().format("Attempting to connect to %s:%u...",
            host.c_str(), port);

    if (this->cIsConnected)
    {
        this->log.warning().raw("connectToServer called, but is already connected to a server");
        return;
    }

    if (!this->cNetMgr->connectToServer(host, port, this->cUserData))
    {
        this->log.error().format("Unable to connect to server %s:%u",
                host.c_str(), port);
        return;
    }

    this->log.debug().format("Connected to %s:%u", host.c_str(), port);

    this->cIsConnected = true;
}


void NetworkApplication::parseCmdArgs()
{
    // If the condition is false, print usage information and
    // exit with a bad status.
    #define FAIL_COND(_COND)   if ((_COND)){ printf("FAIL_COND: '" #_COND "'\n"); printUsage(); exit(-1);}

    // Parse the command line arguments. Exit with a bad
    // status upon misuse.
    auto args = this->getApplicationArguments();
    auto it = args.begin();

    // Ignore the startup command
    it++;

    while (it != args.end())
    {
        std::string arg = *it;

        if (arg == "-c")
        {
            this->isClient= true;
        }
        else if (arg == "-s")
        {
            this->isServer = true;
        }
        else if (arg == "-b")
        {
            FAIL_COND(!this->isServer);
            this->sBroadcast = true;
        }
        else if (arg == "-a")
        {
            FAIL_COND(!this->isServer);
            this->sAutoBan = true;
        }
        else if (arg == "-d")
        {
            FAIL_COND(!this->isServer || !this->sAutoBan);
            it++;
            this->sBanDuration = atoi((*it).c_str());
            FAIL_COND(this->sBanDuration < 0);
        }
        else if (arg == "-con")
        {
            FAIL_COND(!this->isClient || this->cConnectToBroadcast);
            this->cConnect = true;

            it++;
            this->cHostname = *it;
        }
        else if (arg == "-l")
        {
            FAIL_COND(!this->isClient);
            this->cListenToBroadcast = true;
        }
        else if (arg == "-conb")
        {
            FAIL_COND(!this->isClient || this->cConnect);
            this->cListenToBroadcast = true;
            this->cConnectToBroadcast = true;
        }
        else if (arg == "-u")
        {
            FAIL_COND(!this->isClient);
            it++;
            this->cUserData.setUserName(*it);
        }
        else
        {
            FAIL_COND(false);
        }

        it++;
    }

    if (!this->isClient && !this->isServer)
    {
        printf("Must be either client or server.\n");
        printUsage();
        exit(-1);
    }

    #undef FAIL_COND
}

void NetworkApplication::printUsage()
{
    printf("Usage: ./on-01-netsetup (-c [-con <host>|-l|-conb [-u <username>]]) \n");
    printf("                        (-s [-b][-a [-d <dur>]])\n");
    printf("    -c:     Run as a client\n");
    printf("    -con:   Connect to a host\n");
    printf("    -l:     Listen for broadcasts. Enabled with '-conb' as well.\n");
    printf("    -conb:  Connect to the first broadcasting server to appear\n");
    printf("\n");
    printf("    -s:     Run as a server\n");
    printf("    -b:     Broadcast the server's existance to the local network\n");
    printf("    -a:     Automatically ban clients as they join >:)\n");
    printf("    -d:     The duration (in seconds) to ban joining clients for. The\n");
    printf("            duration must be a positive integer.\n");
}


void NetworkApplication::onUpdate(const nox::Duration& deltaTime)
{
	SdlApplication::onUpdate(deltaTime);
}

void NetworkApplication::onDestroy()
{
	this->eventListener.stopListening();
}

void NetworkApplication::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	if (event->isType(nox::logic::net::ServerBroadcastEvent::ID))
	{
        auto bc = (nox::logic::net::ServerBroadcastEvent*)event.get();

        nox::logic::net::ServerConnectionInfo info = bc->getServerConnectionInfo();

        this->log.info().format("Server %sappeared:", (bc->getOnline() ? "" : "dis"));
        this->log.info().format("%s   %s (%s)   %u/%u    %s:%u\n",
                info.serverName.c_str(),
                info.appId.c_str(),
                info.versionId.c_str(),
                info.connectedClients,
                info.maxClients,
                info.hostname.c_str(),
                info.tcpPort);

        if (!this->cIsConnected && this->cConnectToBroadcast)
        {
            this->connectToServer(info.hostname, info.tcpPort);
        }
	}
    else if (event->isType(nox::logic::net::ClientConnected::ID))
    {
        auto con = (nox::logic::net::ClientConnected*)event.get();
        this->log.info().format("Client %s (%u) connected",
                con->getUserData().getUserName().c_str(),
                con->getUserData().getClientId());

        if (this->isServer && this->sAutoBan)
        {
            nox::ClientId id = con->getUserData().getClientId();
            nox::Duration duration(std::chrono::seconds(this->sBanDuration));

            this->log.debug().format("Banning client %s (%u) for %u seconds %s",
                    con->getUserData().getUserName().c_str(),
                    id,
                    this->sBanDuration,
                    (this->sBanDuration ? "" : "(indefinitely)"));
            this->sNetMgr->banClient(id, duration, "Automatic ban. No hard feelings.");
        }
    }
    else if (event->isType(nox::logic::net::ClientDisconnected::ID))
    {
        auto dc = (nox::logic::net::ClientDisconnected*)event.get();
        this->log.info().format("Client %u disconnected", dc->getClientId());
    }
    else if (event->isType(nox::logic::net::ConnectionStarted::ID))
    {
        auto init = (nox::logic::net::ConnectionStarted*)event.get();
        this->log.info().format("Connection started: %s",
                init->getServerHostname().c_str());
    }
    else if (event->isType(nox::logic::net::ConnectionFailed::ID))
    {
        auto fail = (nox::logic::net::ConnectionFailed*)event.get();
        this->log.info().format("Connection to server '%s' failed",
                fail->getServerHostname().c_str());
    }
    else if (event->isType(nox::logic::net::ConnectionSuccess::ID))
    {
        auto success = (nox::logic::net::ConnectionSuccess*)event.get();
        this->log.info().format("Connected successfully to server %s as %s (ClientId %u)",
                success->getServerHostname().c_str(),
                success->getUserData().getUserName().c_str(),
                success->getUserData().getClientId());
    }
}
