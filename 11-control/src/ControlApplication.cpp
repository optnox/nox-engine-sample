/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "ControlApplication.h"

#include <json/value.h>
#include <glm/gtx/string_cast.hpp>
#include <cassert>

ControlApplication::ControlApplication():
	nox::app::SdlApplication("11-control", "SuttungDigital"),
	window(nullptr)
{
}

bool ControlApplication::initializeResourceCache()
{
	const auto cacheSizeMb = 512u;
	auto resourceCache = std::make_unique<nox::app::resource::LruCache>(cacheSizeMb);

	resourceCache->setLogger(this->createLogger());

	// We need to get resources from the project specific assets.
	const auto projectAssetDirectory = this->getName() + "/assets";
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(projectAssetDirectory)) == false)
	{
		this->log.error().format("Could not initialized resource cache to \"%s\".", projectAssetDirectory.c_str());
		return false;
	}

	// We need to get resources from the common assets.
	const auto commonAssetsDirectory = std::string{"assets"};
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(commonAssetsDirectory)) == false)
	{
		this->log.error().format("Could not initialized resource cache to \"%s\".", commonAssetsDirectory.c_str());
		return false;
	}

	// We need to get resources from the NOX assets.
	const auto noxAssetsDirectory = std::string{"nox-engine/assets"};
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(noxAssetsDirectory)) == false)
	{
		this->log.error().format("Could not initialized resource cache to \"%s\".", noxAssetsDirectory.c_str());
		return false;
	}

	resourceCache->addLoader(std::make_unique<nox::app::resource::JsonLoader>(this->createLogger()));
	resourceCache->addLoader(std::make_unique<nox::app::resource::OggLoader>());

	this->setResourceCache(std::move(resourceCache));

	return true;
}

nox::logic::Logic* ControlApplication::initializeLogic()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	auto logicPtr = logic.get();

	this->addProcess(std::move(logic));

	return logicPtr;
}

nox::logic::physics::PhysicsSimulation* ControlApplication::initializePhysics(nox::logic::Logic* logic)
{
	auto physics = std::make_unique<nox::logic::physics::Box2DSimulation>(logic);
	physics->setLogger(this->createLogger());

	auto physicsPtr = physics.get();

	logic->setPhysics(std::move(physics));

	return physicsPtr;
}

nox::logic::world::WorldManager* ControlApplication::initializeWorldManager(nox::logic::Logic* logic)
{
	auto world = std::make_unique<nox::logic::world::WorldManager>(logic);

	world->registerActorComponent<nox::logic::actor::Transform>();
	world->registerActorComponent<nox::logic::physics::ActorPhysics>();
	world->registerActorComponent<nox::logic::graphics::ActorSprite>();
	world->registerActorComponent<nox::logic::graphics::ActorLight>();
	world->registerActorComponent<nox::logic::audio::ActorAudio>();

	/*
	 * Register components to control Actors.
	 * These components will listen to control::Action events and respond to their data
	 * by either applying a force in the control direction or rotating in the control direction.
	 * The control::Action they receive is sent from ControllingWindowView. See that for more info.
	 */
	world->registerActorComponent<nox::logic::control::Actor2dDirectionControl>();
	world->registerActorComponent<nox::logic::control::Actor2dRotationControl>();

	const auto actorDirectory = std::string{"actor"};
	world->loadActorDefinitions(this->getResourceAccess(), actorDirectory);

	auto worldPtr = world.get();

	logic->setWorldManager(std::move(world));

	return worldPtr;
}

bool ControlApplication::loadWorldFile(nox::logic::ILogicContext* logicContext, nox::logic::world::WorldManager* worldManager)
{
	const auto worldFileDescriptor = nox::app::resource::Descriptor{"world/controllableWorld.json"};
	const auto worldFileHandle = this->getResourceAccess()->getHandle(worldFileDescriptor);

	if (worldFileHandle == nullptr)
	{
		this->log.error().format("Could not load world: %s", worldFileDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		const auto jsonData = worldFileHandle->getExtraData<nox::app::resource::JsonExtraData>();

		if (jsonData == nullptr)
		{
			this->log.error().format("Could not get JSON data for world: %s", worldFileDescriptor.getPath().c_str());
			return false;
		}
		else
		{
			auto loader = nox::logic::world::WorldLoader{logicContext};

			/*
			 * We register the window as a controlling View on index 0. This means that a View
			 * in the world JSON file can refer to this window by using index 0. See the controllableWorld.json file for more.
			 */
			loader.registerControllingView(0, this->window);

			if (loader.loadWorld(jsonData->getRootValue(), worldManager) == false)
			{
				this->log.error().format("Failed loading world \"%s\".", worldFileDescriptor.getPath().c_str());
				return false;
			}
		}
	}

	this->log.verbose().format("Loaded world \"%s\"", worldFileDescriptor.getPath().c_str());

	return true;
}

void ControlApplication::initializeWindow(nox::logic::Logic* logic)
{
	auto window = std::make_unique<ControllingWindowView>(this, this->getName());

	this->window = window.get();

	logic->addView(std::move(window));
}

bool ControlApplication::onInit()
{
	if (this->SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("ControlApplication");

	if (this->initializeResourceCache() == false)
	{
		this->log.error().raw("Failed initializing resource cache.");
		return false;
	}

	auto logic = this->initializeLogic();
	auto eventBroadcaster = logic->getEventBroadcaster();

	this->initializePhysics(logic);
	auto worldManager = this->initializeWorldManager(logic);

	if (this->createDefaultAudioSystem() == false)
	{
		return false;
	}

	this->initializeWindow(logic);

	if (this->loadWorldFile(logic, worldManager) == false)
	{
		return false;
	}

	logic->pause(false);

	return true;
}

void ControlApplication::onUpdate(const nox::Duration& deltaTime)
{
	this->SdlApplication::onUpdate(deltaTime);

	assert(this->window != nullptr);
	this->window->render();
}

void ControlApplication::onSdlEvent(const SDL_Event& event)
{
	this->SdlApplication::onSdlEvent(event);

	assert(this->window != nullptr);
	this->window->onSdlEvent(event);
}
