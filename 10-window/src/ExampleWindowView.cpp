/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "ExampleWindowView.h"

#include <nox/nox.h>

ExampleWindowView::ExampleWindowView(nox::app::IApplicationContext* applicationContext, const std::string& windowTitle):
	nox::window::RenderSdlWindowView(applicationContext, windowTitle)
{
	this->log = applicationContext->createLogger();
	this->log.setName("ExampleWindowView");
}

/*
 * This is called when the renderer is created and lets us set up the renderer to our preferences.
 */
void ExampleWindowView::onRendererCreated(nox::app::graphics::IRenderer* renderer)
{
	/*
	 * Here we set up the renderer to our preferences.
	 */

	assert(renderer != nullptr);

	/*
	 * The graphics.json file describes all the texture atlases that must be loaded. It is passed to the renderer
	 * so that it can load all of the atlases into graphics memory.
	 */
	const auto graphicsResourceDescriptor = nox::app::resource::Descriptor{"graphics/graphics.json"};
	renderer->loadTextureAtlases(graphicsResourceDescriptor, this->getApplicationContext()->getResourceAccess());

	/*
	 * The renderer has to have one atlas that is used to render the world objects. We set this to
	 * "graphics/testTextureAtlas" as this is where all our textures are. The name passed must also be found
	 * in the graphics.json file that was loaded.
	 */
	renderer->setWorldTextureAtlas("graphics/testTextureAtlas");

	/*
	 * We create a simple background gradient that will be renderered at the very back and is always relative to the window, not the world.
	 */
	auto background = std::make_unique<nox::app::graphics::BackgroundGradient>();
	background->setBottomColor({0.0f, 0.0f, 0.0f});
	background->setTopColor({1.0f, 1.0f, 1.0f});
	renderer->setBackgroundGradient(std::move(background));

	// We want a fully lit world so we set it to 1.0. Setting this to 0.0 will make it completely dark.
	renderer->setAmbientLightLevel(1.0f);

	/*
	 * This has to be called so that the renderer properly can organize its render steps for the rendering.
	 * (I should really make this automated...)
	 */
	renderer->organizeRenderSteps();

	this->getCamera()->setScale({60.f, 60.f});
}

void ExampleWindowView::onKeyPress(const SDL_KeyboardEvent& event)
{
	/*
	 * This enables more debug data to be renderer, e.g. the physics shapes.
	 */
	if (event.keysym.sym == SDLK_q)
	{
		this->getRenderer()->toggleDebugRendering();

		// The logic should know that we enable/disable the debug rendering so that it doesn't need to use time to update the
		// rendering.
		const auto debugRenderEvent = std::make_shared<nox::logic::graphics::DebugRenderingEnabled>(this->getRenderer()->isDebugRenderingEnabled());
		this->getLogicContext()->getEventBroadcaster()->queueEvent(debugRenderEvent);
	}
}
