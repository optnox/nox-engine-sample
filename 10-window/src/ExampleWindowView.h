/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef EXAMPLEWINDOWVIEW_H_
#define EXAMPLEWINDOWVIEW_H_

#include <nox/nox.h>

/*
 * This window view inherits from the nox::window::RenderSdlWindowView.
 * The nox::window::RenderSdlWindowView manages a window with SDL2 and runs a nox::app::graphics::OpenGlRenderer
 * to render things to the screen.
 */
class ExampleWindowView final: public nox::window::RenderSdlWindowView
{
public:
	/**
	 * Create an example window view.
	 * @param applicationContext The context that it is created in. This is needed to create a logger and pass it down
	 * the inheritance tree.
	 * @param windowTitle The tilte that will appear on top of the window.
	 */
	ExampleWindowView(nox::app::IApplicationContext* applicationContext, const std::string& windowTitle);

private:
	void onRendererCreated(nox::app::graphics::IRenderer* renderer) override;

	void onKeyPress(const SDL_KeyboardEvent& event) override;

	nox::app::log::Logger log;
};

#endif
