/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef WINDOWAPPLICATION_H_
#define WINDOWAPPLICATION_H_

#include "ExampleWindowView.h"

#include <nox/nox.h>

/*
 * This example shows how to create a window and render actors to it.
 *
 * You should see a window pop up with the NOX Engine logo sprite moving and rotating.
 * The actor you see is the exact same as the one from 09-world, but with a sprite.
 * If you press Q you can also see the physics shape being rendered.
 *
 * See the WindowApplication.cpp file for more.
 */
class WindowApplication: public nox::app::SdlApplication
{
public:
	WindowApplication();

	bool onInit() override;
	void onUpdate(const nox::Duration& deltaTime) override;
	void onSdlEvent(const SDL_Event& event) override;

private:
	bool initializeResourceCache();
	nox::logic::Logic* initializeLogic();
	nox::logic::physics::PhysicsSimulation* initializePhysics(nox::logic::Logic* logic);
	nox::logic::world::WorldManager* initializeWorldManager(nox::logic::Logic* logic);

	void initializeWindow(nox::logic::Logic* logic);

	bool loadWorldFile(nox::logic::ILogicContext* logicContext, nox::logic::world::WorldManager* worldManager);

	nox::app::log::Logger log;

	// Store the window so we can update it.
	ExampleWindowView* window;
};

#endif
