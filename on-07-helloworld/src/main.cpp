#include <nox/nox.h>

class MyGame: public nox::game::GameController
{
public:
	std::vector<std::string> getResourceCacheDirectories() const override
	{
		return std::vector<std::string>{"on-06-helloworld/assets", "nox-engine/assets", "assets"};
	}

	std::vector<std::string> getActorDefinitionSubdirectories() const override
	{
		return std::vector<std::string>{"actor/"};
	}

	std::vector<std::string> getWorldAtlasDefinitionFiles() const override
	{
		return std::vector<std::string>{"graphics/graphics.json"};
	}

	std::string getWorldAtlasName() const override
	{
		return "graphics/testTextureAtlas";
	}

	bool onInit() override
	{
		// Set up a control scheme for the controlled actor
		this->getGameView()->setControlScheme(std::make_unique<nox::window::SdlKeyboardControlMapper>());
		if (this->getGameView()->loadControlDefinitions("controls.json") == false)
		{
			return false;
		}

		if (!this->loadWorld("world.json"))
		{
			return false;
		}

		this->getCamera()->setScale(glm::vec2(60.f, 60.f));
		this->getRenderer()->setAmbientLightLevel(0.05f);

		// Create a couple of dummy actors and add some components to it
		for (int i=0; i<4; i++)
		{
			nox::logic::actor::Actor* actor = this->createActor("Programmatically Created Actor");
			actor->addComponent<nox::logic::actor::Transform>();
			actor->addComponent<nox::logic::graphics::ActorSprite>();

			// Assign our actor a nice texture
			actor->findComponent<nox::logic::graphics::ActorSprite>()->setActiveSprite("oldFaithful.png");
			actor->findComponent<nox::logic::graphics::ActorSprite>()->setRenderLevel(10);

			// The Transform position must be assigned *after* all dependent Components has been added.
			float x = (i < 2) ? -1.f :  1.f;
			float y = (i % 2) ?  1.f : -1.f;
			actor->findComponent<nox::logic::actor::Transform>()->setPosition(glm::vec2(x, y));
		}

		return true;
	}

	void onUpdate(const nox::Duration& /*deltaTime*/)
	{
		if (this->getGameView() != nullptr)
		{
			nox::logic::actor::Actor* actor = this->getGameView()->getControlledActor();
			nox::app::graphics::Camera* camera = this->getGameView()->getCamera();

			if (actor != nullptr && camera != nullptr)
			{
				camera->setPosition(actor->findComponent<nox::logic::actor::Transform>()->getPosition());
				camera->setRotation(actor->findComponent<nox::logic::actor::Transform>()->getRotation());
			}
		}
	}
};


int main(int argc, char **argv)
{
	MyGame game;
	return game.execute(argc, argv);
}
