/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "AudioApplication.h"

#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/loader/OggLoader.h>
#include <nox/app/resource/data/SoundExtraData.h>
#include <nox/app/audio/AudioSystem.h>

#include <json/value.h>
#include <cassert>

AudioApplication::AudioApplication():
	SdlApplication("05-audio", "SuttungDigital")
{
}

/*
 * We update the resource cache like in the previous example, so that we can get the sound resource.
 * This version uses an OggLoader in stead of the JsonLoader, so that it can load ogg/vorbis files.
 */
bool AudioApplication::initializeResourceCache()
{
	const auto cacheSizeMb = 512u;
	auto resourceCache = std::make_unique<nox::app::resource::LruCache>(cacheSizeMb);

	resourceCache->setLogger(this->createLogger());

	const auto assetDirectory = std::string{"assets/"};
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(assetDirectory)) == false)
	{
		return false;
	}

	resourceCache->addLoader(std::make_unique<nox::app::resource::OggLoader>());

	this->setResourceCache(std::move(resourceCache));

	return true;
}

bool AudioApplication::onInit()
{
	if (SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("AudioApplication");

	if (this->initializeResourceCache() == false)
	{
		this->log.error().raw("Failed to initialize resource cache.");
		return false;
	}

	/*
	 * Initialize the audio system and return false if it failed.
	 */
	if (this->createDefaultAudioSystem() == false)
	{
		this->log.error().raw("Failed to initialize audio system.");
		return false;
	}

	std::string fileName = "sound.ogg";
	if (this->getAudioSystem()->playSound(fileName))
	{
		this->log.debug().format("Playing sound %s", fileName.c_str());
	}
	else
	{
		this->log.error().format("Could not load resource: %s", fileName.c_str());
	}

	return true;
}
