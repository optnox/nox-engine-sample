/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef CONTROLLINGEWINDOWVIEW_H_
#define CONTROLLINGEWINDOWVIEW_H_

#include <nox/nox.h>

/*
 * This class is the same as ExampleWindowView from 10-window, but has some changes
 * to be able to control Actors. See the comments for the changes.
 *
 * The most important thing here is the controlMapper member variable.
 */
class ControllingWindowView final: public nox::window::RenderSdlWindowView
{
public:
	ControllingWindowView(nox::app::IApplicationContext* applicationContext, const std::string& windowTitle);

private:
	void onRendererCreated(nox::app::graphics::IRenderer* renderer) override;
	void onControlledActorChanged(nox::logic::actor::Actor* controlledActor) override;
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	bool initialize(nox::logic::ILogicContext* context) override;
	void onWindowSizeChanged(const glm::uvec2& size) override;
	void onKeyPress(const SDL_KeyboardEvent& event) override;
	void onKeyRelease(const SDL_KeyboardEvent& event) override;

	nox::app::log::Logger log;
	nox::logic::event::EventListenerManager listener;

	// This is the object that will map all our key inputs to control::Action events.
	nox::window::SdlKeyboardControlMapper controlMapper;

	// We need to be able to broadcast our control::Action events.
	nox::logic::event::IEventBroadcaster* eventBroadcaster;

	nox::app::graphics::IRenderer* renderer;
};

#endif
