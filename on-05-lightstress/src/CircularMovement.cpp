#include "CircularMovement.h"


const nox::logic::actor::Component::IdType CircularMovement::NAME = "CircularMovement";

CircularMovement::CircularMovement():
	hasOriginalPosition(false)
{
}

void CircularMovement::onUpdate(const nox::Duration& deltaTime)
{
	elapsedTime += deltaTime;

	const unsigned ms = std::chrono::duration_cast<std::chrono::milliseconds>(elapsedTime).count();
	const float seconds = (float)ms / 1000.f;

	auto transform = this->getOwner()->findComponent<nox::logic::actor::Transform>();
	if (transform != nullptr)
	{
		if (!this->hasOriginalPosition)
		{
			this->originalPosition = transform->getPosition();
			this->hasOriginalPosition = true;
		}

		glm::vec2 pos(std::cos(seconds), std::sin(seconds) * 2.f);
		pos += this->originalPosition;
		transform->setPosition(pos);
	}
}

void CircularMovement::initialize(const Json::Value&)
{
}

void CircularMovement::serialize(Json::Value& json)
{
}

const nox::logic::actor::Component::IdType& CircularMovement::getName() const
{
	return NAME;
}
