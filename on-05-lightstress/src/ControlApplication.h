/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef CONTROLAPPLICATION_H_
#define CONTROLAPPLICATION_H_

#include <nox/nox.h>
#include "ControllingWindowView.h"

/*
 * This example shows how to control Actors in a world.
 *
 * When running the application, you can move the Suttung logo around by using the WASD keys and rotate it
 * by using the Q and E keys.
 *
 * This is based on 10-window and nothing much has changed in this class. In loadWorldFile() the world::Loader
 * now registers the ControllingWindowView at index 0. The ControllingWindowView has gone through some changes
 * so that it can control an Actor.
 *
 * See the ControlApplication.cpp and ControllingWindowView.h/cpp files for more.
 */
class ControlApplication: public nox::app::SdlApplication
{
public:
	ControlApplication();

	bool onInit() override;
	void onUpdate(const nox::Duration& deltaTime) override;
	void onSdlEvent(const SDL_Event& event) override;

private:
	bool initializeResourceCache();
	nox::logic::Logic* initializeLogic();
	nox::logic::physics::PhysicsSimulation* initializePhysics(nox::logic::Logic* logic);
	nox::logic::world::WorldManager* initializeWorldManager(nox::logic::Logic* logic);

	void initializeWindow(nox::logic::Logic* logic);

	bool loadWorldFile(nox::logic::ILogicContext* logicContext, nox::logic::world::WorldManager* worldManager);

	nox::app::log::Logger log;

	ControllingWindowView* window;
};

#endif
