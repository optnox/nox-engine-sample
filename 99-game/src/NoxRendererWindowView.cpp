/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "NoxRendererWindowView.h"


NoxRendererWindowView::NoxRendererWindowView(nox::app::IApplicationContext* applicationContext, const std::string& windowTitle):
	nox::window::RenderSdlWindowView(applicationContext, windowTitle),
	appContext(applicationContext),
	listener("NoxRendererWindowView"),
	eventBroadcaster(nullptr),
	renderer(nullptr),
	mouseJointId(-1),
	cameraPanning(false),
	cameraZoomSpeed(0.1f)
{
	this->log = applicationContext->createLogger();
	this->log.setName("NoxRendererWindowView");

	this->listener.addEventTypeToListenFor(nox::logic::actor::TransformChange::ID);

	this->getCamera()->setScale({60.0f, 60.0f});
	this->rootSceneNode = std::make_shared<nox::app::graphics::TransformationNode>();

	// Assign the camera as the ListenerPositionProvider. The camera will be queried once per frame
	// to assign it's current transform to the audio::System's listener.
	this->appContext->getAudioSystem()->setListenerPositionProvider(this->getCamera());
}

bool NoxRendererWindowView::initialize(nox::logic::ILogicContext* context)
{
	if (this->RenderSdlWindowView::initialize(context) == false)
	{
		return false;
	}

	this->listener.setup(this, context->getEventBroadcaster(), nox::logic::event::EventListenerManager::StartListening_t());

	this->eventBroadcaster = context->getEventBroadcaster();

	auto resourceAccess = this->getApplicationContext()->getResourceAccess();

	auto controlLayoutResourceDescriptor = nox::app::resource::Descriptor{"controls.json"};
	auto controlLayoutResource = resourceAccess->getHandle(controlLayoutResourceDescriptor);

	if (controlLayoutResource == nullptr)
	{
		this->log.error().format("Failed loading control layout resource \"%s\".", controlLayoutResourceDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		this->controlMapper.loadKeyboardLayout(controlLayoutResource);
	}

	return true;
}

void NoxRendererWindowView::onRendererCreated(nox::app::graphics::IRenderer* renderer)
{
	assert(renderer != nullptr);

	const auto graphicsResourceDescriptor = nox::app::resource::Descriptor{"graphics/graphics.json"};
	renderer->loadTextureAtlases(graphicsResourceDescriptor, this->getApplicationContext()->getResourceAccess());
	renderer->setWorldTextureAtlas("graphics/testTextureAtlas");

	auto background = std::make_unique<nox::app::graphics::BackgroundGradient>();
	background->setBottomColor({0.0f, 0.0f, 0.0f});
	background->setTopColor({1.0f, 1.0f, 1.0f});
	renderer->setBackgroundGradient(std::move(background));

	renderer->setAmbientLightLevel(0.1f);
	renderer->organizeRenderSteps();
	this->getCamera()->setScale(glm::vec2(60.f, 60.f));
}

void NoxRendererWindowView::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	this->RenderSdlWindowView::onEvent(event);

	if (event->isType(nox::logic::actor::TransformChange::ID))
	{
		const auto transformEvent = static_cast<nox::logic::actor::TransformChange*>(event.get());

		if (transformEvent->getActor() == this->getControlledActor())
		{
			this->getCamera()->setPosition(transformEvent->getPosition());
			this->getCamera()->setRotation(transformEvent->getRotation());
		}
	}
}

void NoxRendererWindowView::onMousePress(const SDL_MouseButtonEvent& event)
{
	if (event.button == SDL_BUTTON_LEFT && this->mouseJointId < 0)
	{
		auto physics = this->getLogicContext()->getPhysics();

		const glm::vec2 clickPos = this->convertMouseToWorld(glm::ivec2(event.x, event.y));

		const auto actorClicked = physics->findActorIntersectingPoint(clickPos, nox::logic::physics::allBodyTypes());

		if (actorClicked != nullptr)
		{
			auto actorPhysics = actorClicked->findComponent<nox::logic::physics::ActorPhysics>();

			if (SDL_GetModState() & KMOD_CTRL)
			{
				if (actorPhysics != nullptr && actorPhysics->isDynamic())
				{
					nox::logic::physics::TargetJointDefinition jointDefinition(actorClicked->getId(), clickPos, 1000.0f * actorPhysics->getMass());
					this->mouseJointId = physics->createTargetJoint(jointDefinition);
				}
			}
			else
			{
				this->setControlledActor(actorClicked);
				this->controlMapper.setControlledActor(this->getControlledActor());
			}
		}
		else
		{
			this->setControlledActor(nullptr);
			this->controlMapper.setControlledActor(this->getControlledActor());
		}
	}
	else if (event.button == SDL_BUTTON_RIGHT && this->getControlledActor() == nullptr)
	{
		this->cameraPanning = true;
	}
}

void NoxRendererWindowView::onMouseRelease(const SDL_MouseButtonEvent& event)
{
	if (event.button == SDL_BUTTON_LEFT && this->mouseJointId >= 0)
	{
		auto physics = this->getLogicContext()->getPhysics();
		physics->removeJoint(this->mouseJointId);
		this->mouseJointId = -1;
	}
	else if (event.button == SDL_BUTTON_RIGHT)
	{
		this->cameraPanning = false;
	}
}

void NoxRendererWindowView::onMouseMove(const SDL_MouseMotionEvent& event)
{
	if (this->mouseJointId >= 0)
	{
		auto physics = this->getLogicContext()->getPhysics();
		const glm::vec2 clickPos = this->convertMouseToWorld(glm::ivec2(event.x, event.y));

		physics->setTargetJointTargetPosition(this->mouseJointId, clickPos);
	}

	if (this->cameraPanning == true)
	{
		const glm::vec2 mouseMove = glm::vec2(event.xrel, event.yrel * -1);

		glm::vec2 currentCameraPosition = this->getCamera()->getPosition();

		glm::vec4 mouseRelativeMotion(0.0f, 0.0f, 0.0f, 1.0f);
		mouseRelativeMotion.x = mouseMove.x / this->getCamera()->getScale().x;
		mouseRelativeMotion.y = mouseMove.y / this->getCamera()->getScale().y;

		const float cameraRotation = this->getCamera()->getRotation();
		glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(1), cameraRotation, glm::vec3(0.0, 0.0, 1.0));
		glm::vec4 cameraVector = rotationMatrix * mouseRelativeMotion;

		currentCameraPosition.x -= cameraVector.x;
		currentCameraPosition.y -= cameraVector.y;

		this->getCamera()->setPosition(currentCameraPosition);
	}
}

void NoxRendererWindowView::onMouseScroll(const SDL_MouseWheelEvent& event)
{
	if (event.y != 0)
	{
		const auto zoom = static_cast<float>(event.y);

		const auto previousScale = this->getCamera()->getScale();
		const auto scaleChange = zoom * this->cameraZoomSpeed * glm::length(previousScale);

		this->getCamera()->setScale(previousScale + scaleChange);
	}
}

void NoxRendererWindowView::onKeyPress(const SDL_KeyboardEvent& event)
{
	if (event.keysym.sym == SDLK_q && (event.keysym.mod & KMOD_CTRL))
	{
		this->getRenderer()->toggleDebugRendering();

		const auto debugRenderEvent = std::make_shared<nox::logic::graphics::DebugRenderingEnabled>(this->getRenderer()->isDebugRenderingEnabled());
		this->getLogicContext()->getEventBroadcaster()->queueEvent(debugRenderEvent);
	}
	else if (event.keysym.sym == SDLK_f)
	{
		if (this->isFullscreen())
		{
			this->disableFullscreen();
		}
		else
		{
			this->enableFullscreen();
		}
	}

	if (this->controlMapper.hasControlledActor() == true)
	{
		auto controlEvents = this->controlMapper.mapKeyPress(event.keysym);

		if (this->eventBroadcaster != nullptr)
		{
			for (const auto& event : controlEvents)
			{
				this->eventBroadcaster->queueEvent(event);
			}
		}
	}
}

void NoxRendererWindowView::onKeyRelease(const SDL_KeyboardEvent& event)
{
	if (this->controlMapper.hasControlledActor() == true)
	{
		auto controlEvents = this->controlMapper.mapKeyRelease(event.keysym);

		if (this->eventBroadcaster != nullptr)
		{
			for (const auto& event : controlEvents)
			{
				this->eventBroadcaster->queueEvent(event);
			}
		}
	}
}

glm::vec2 NoxRendererWindowView::convertMouseToWorld(const glm::ivec2& mousePos)
{
	// Translate to center of screen.
	glm::vec4 convertedCoordinate(
			static_cast<float>(mousePos.x) - static_cast<float>(this->getWindowSize().x) / 2.0f,
			static_cast<float>((mousePos.y - static_cast<int>(this->getWindowSize().y)) * -1) - static_cast<float>(this->getWindowSize().y) / 2.0f,
			1.0f,
			1.0f
	);

	// Scale to world units.
	convertedCoordinate.x /= this->getCamera()->getScale().x;
	convertedCoordinate.y /= this->getCamera()->getScale().y;

	// Rotate coordinates with the inverted camera rotation.
	float currentRotation = this->getCamera()->getRotation();
	glm::mat4 rotationMatrix = glm::rotate(glm::mat4(1.0f), currentRotation, glm::vec3(0.0f, 0.0f, 1.0f));
	convertedCoordinate = rotationMatrix * convertedCoordinate;

	// Translate coordinates to inverse camera position.
	glm::vec2 currentCameraPosition = this->getCamera()->getPosition();
	convertedCoordinate.x += currentCameraPosition.x;
	convertedCoordinate.y += currentCameraPosition.y;

	return glm::vec2(convertedCoordinate);
}
