/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "SampleApplication.h"


namespace event
{

const nox::logic::event::Event::IdType TPS_UPDATE = "game.tps_update";

}


SampleApplication::SampleApplication():
	nox::app::SdlApplication("Sample Game", "Suttung Digital"),
	listener(this->getName()),
	logic(nullptr),
	world(nullptr),
	window(nullptr)
{
}

bool SampleApplication::initializeResourceCache()
{
	const auto cacheSize = 512u;
	auto cache = std::make_unique<nox::app::resource::LruCache>(cacheSize);

	cache->setLogger(this->createLogger());

	this->logger.verbose().format("LRU Cache initialized with a size of %uMB", cacheSize);

	const auto engineAssetsDir = std::string{"nox-engine/assets"};
	const auto gameAssetsDir = std::string{"99-game/assets"};
	const auto exampleAssetsDir = std::string{"assets"};

	if (cache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(engineAssetsDir)) == false)
	{
		this->logger.error().format("Could not create resource provider for engine assets: %s", engineAssetsDir.c_str());
		return false;
	}
	else
	{
		this->logger.verbose().format("Initialized filesystem resource provider for directory \"%s\"", engineAssetsDir.c_str());
	}

	if (cache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(gameAssetsDir)) == false)
	{
		this->logger.error().format("Could not create resource provider for game assets: %s", gameAssetsDir.c_str());
		return false;
	}
	else
	{
		this->logger.verbose().format("Initialized filesystem resource provider for directory \"%s\"", gameAssetsDir.c_str());
	}

	if (cache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(exampleAssetsDir)) == false)
	{
		this->logger.error().format("Could not create resource provider for example assets: %s", exampleAssetsDir.c_str());
		return false;
	}
	else
	{
		this->logger.verbose().format("Initialized filesystem resource provider for directory \"%s\"", exampleAssetsDir.c_str());
	}

	cache->addLoader(std::make_unique<nox::app::resource::OggLoader>());
	this->logger.verbose().raw("Initialized ogg resource loader.");

	cache->addLoader(std::make_unique<nox::app::resource::JsonLoader>(this->createLogger()));
	this->logger.verbose().raw("Initialized JSON resource loader.");

	this->setResourceCache(std::move(cache));

	return true;
}

bool SampleApplication::initializeDataStorage()
{
	auto dataStorage = std::make_unique<nox::app::storage::DataStorageBoost>();
	const std::string storageDir = this->getStorageDirectoryPath(false);

	if (storageDir.empty() == true)
	{
		this->logger.error().raw("Could not find a suitable storage directory.");
		return false;
	}
	else
	{
		try
		{
			dataStorage->initialize(storageDir);

			this->logger.verbose().format("Data storage initialized to \"%s\" directory.", storageDir.c_str());
			this->setDataStorage(std::move(dataStorage));
		}
		catch (nox::app::storage::DataStorageBoost::IntializationException& exception)
		{
			this->logger.error().format("Could not initialize data storage directory \"%s\": %s", storageDir.c_str(), exception.what());
			return false;
		}
	}

	return true;
}

bool SampleApplication::initializeLogic()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	this->logic = logic.get();

	this->addProcess(std::move(logic));

	auto physics = std::make_unique<nox::logic::physics::Box2DSimulation>(this->logic);
	physics->setLogger(this->createLogger());
	this->logic->setPhysics(std::move(physics));

	auto worldHandler = std::make_unique<nox::logic::world::WorldManager>(this->logic);
	this->world = worldHandler.get();

	this->logic->setWorldManager(std::move(worldHandler));

	this->world->registerActorComponent<nox::logic::actor::Transform>();
	this->world->registerActorComponent<nox::logic::physics::ActorPhysics>();
	this->world->registerActorComponent<nox::logic::physics::ActorGravitation>();
	this->world->registerActorComponent<nox::logic::graphics::ActorSprite>();
	this->world->registerActorComponent<nox::logic::graphics::ActorLight>();
	this->world->registerActorComponent<nox::logic::control::Actor2dDirectionControl>();
	this->world->registerActorComponent<nox::logic::control::Actor2dRotationControl>();

	auto actorDir = std::string{"actor"};
	this->logger.verbose().format("Loading actor definitions from resource directory \"%s\"", actorDir.c_str());
	this->world->loadActorDefinitions(this->getResourceAccess(), actorDir);

	this->logger.verbose().raw("Initialized logic");

	return true;
}

bool SampleApplication::initializeWindow()
{
	this->logger.verbose().raw("Initializing SDL video subsystem.");

	if (this->initializeSdlSubsystem(SDL_INIT_VIDEO) == false)
	{
		return false;
	}

	auto window = std::make_unique<NoxRendererWindowView>(this, this->getName());
	this->window = window.get();

	this->logic->addView(std::move(window));

	this->logger.verbose().raw("Initialized NoxRendererWindowView.");

	return true;
}

bool SampleApplication::loadWorld()
{
	const auto worldFileDescriptor = nox::app::resource::Descriptor{"world/sample.json"};
	const auto worldFileHandle = this->getResourceAccess()->getHandle(worldFileDescriptor);

	if (worldFileHandle == nullptr)
	{
		this->logger.error().format("Could not load world: %s", worldFileDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		const auto jsonData = worldFileHandle->getExtraData<nox::app::resource::JsonExtraData>();

		if (jsonData == nullptr)
		{
			this->logger.error().format("Could not get JSON data for world: %s", worldFileDescriptor.getPath().c_str());
			return false;
		}
		else
		{
			auto loader = nox::logic::world::WorldLoader{this->logic};
			loader.registerControllingView(0, this->window);

			if (loader.loadWorld(jsonData->getRootValue(), this->world) == false)
			{
				this->logger.error().format("Failed loading world \"%s\".", worldFileDescriptor.getPath().c_str());
				return false;
			}
		}
	}

	this->logger.verbose().format("Loaded world \"%s\"", worldFileDescriptor.getPath().c_str());

	return true;
}

void SampleApplication::playTestSound()
{
	// Play a sound via event. Simple options such as PlayMode and dedicated PositionBufferProvider can
	// be assigned, but advanced properties may not.
	auto event = std::make_shared<nox::logic::audio::PlaySound>("testSound.ogg");
	this->logic->getEventBroadcaster()->queueEvent(event);

	// Play a sound via direct interaction. More attributes can be set, should this is desired.
	auto soundBuffer = this->getAudioSystem()->playSound("bling.ogg", nox::app::audio::PlayMode::POSITIONAL);
	if (soundBuffer)
	{
		soundBuffer->setPosition(glm::vec2(0.f, 0.f));
		soundBuffer->enableLooping(true);
		soundBuffer->setMinimumDistance(1.f);
		soundBuffer->setMaximumDistance(100.f);
		soundBuffer->setStrength(2.f);
	}
}

bool SampleApplication::onInit()
{
	this->SdlApplication::onInit();

	this->logger = this->createLogger();
	this->logger.setName("SampleApplication");

	this->setTpsUpdateInterval(std::chrono::milliseconds(500));
	this->tpsUpdateTimer.setTimerLength(std::chrono::milliseconds(500));

	if (this->initializeResourceCache() == false)
	{
		return false;
	}

	if (this->initializeDataStorage() == false)
	{
		return false;
	}

	if (this->createDefaultAudioSystem() == false)
	{
		return false;
	}

	if (this->initializeLogic() == false)
	{
		return false;
	}

	if (this->initializeWindow() == false)
	{
		return false;
	}

	if (this->loadWorld() == false)
	{
		return false;
	}

	this->listener.setup(this, this->logic->getEventBroadcaster(), nox::logic::event::EventListenerManager::StartListening_t());
	this->listener.addEventTypeToListenFor(event::TPS_UPDATE);

	this->playTestSound();

	this->logic->pause(false);

	return true;
}

void SampleApplication::onDestroy()
{
	this->SdlApplication::onDestroy();

	this->listener.stopListening();
	this->logic = nullptr;

	this->logger.verbose().raw("Destroyed");
}

void SampleApplication::onUpdate(const nox::Duration& deltaTime)
{
	this->SdlApplication::onUpdate(deltaTime);

	this->tpsUpdateTimer.spendTime(deltaTime);

	if (this->tpsUpdateTimer.timerReached() == true)
	{
		this->logger.info().format("TPS: %.4f", this->getTps());
		this->tpsUpdateTimer.reset();

		this->logic->getEventBroadcaster()->queueEvent(event::TPS_UPDATE);
	}

	this->processManager.updateProcesses(deltaTime);

	if (this->window != nullptr)
	{
		this->window->render();
	}
}

void SampleApplication::onSdlEvent(const SDL_Event& event)
{
	this->SdlApplication::onSdlEvent(event);

	this->window->onSdlEvent(event);
}

void SampleApplication::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	if (event->isType(event::TPS_UPDATE))
	{
		this->logger.debug().raw("TPS Update received.");
	}
	else if (event->isType(nox::logic::event::EventManager::BROADCAST_COMPLETE_EVENT))
	{
		this->logger.debug().raw("Event broadcast complete received.");
	}
}
