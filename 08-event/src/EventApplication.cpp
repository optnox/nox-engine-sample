/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "EventApplication.h"
#include "TpsUpdated.h"

#include <json/value.h>
#include <cassert>
#include <random>

namespace
{

/*
 * The APP_INITIALIZED event is an event without any data, so we don't need to subclass nox::Event.
 * See TpsUpdated.h for more explanation about the IdType.
 */
const nox::logic::event::Event::IdType APP_INITIALIZED = "08-event.app_initialized";

}

EventApplication::EventApplication():
	SdlApplication("08-event", "SuttungDigital"),
	eventListener(this->getName()),
	eventBroadcaster(nullptr)
{
}

/*
 * Initialize the logic just as before, but return the pointer to it.
 */
nox::logic::Logic* EventApplication::initializeLogic()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	auto logicPtr = logic.get();

	this->addProcess(std::move(logic));

	return logicPtr;
}

bool EventApplication::onInit()
{
	if (SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("EventApplication");

	/*
	 * Update the TPS each 500ms
	 */
	const auto tpsUpdateTime = std::chrono::milliseconds(500);
	this->setTpsUpdateInterval(tpsUpdateTime);
	this->tpsUpdateTimer.setTimerLength(tpsUpdateTime);

	/*
	 * The event::Manager is executed from the Logic, so we need to initialize it.
	 */
	auto logic = this->initializeLogic();

	/*
	 * Get the broadcaster so that we can use it.
	 */
	this->eventBroadcaster = logic->getEventBroadcaster();

	// Setup our ListenerManager to manage this listening to this->eventBroadcaster.
	this->eventListener.setup(this, this->eventBroadcaster);

	// Listen to APP_INITIALIZED and TpsUpdated::ID.
	this->eventListener.addEventTypeToListenFor(APP_INITIALIZED);
	this->eventListener.addEventTypeToListenFor(TpsUpdated::ID);

	// Start listening to the events.
	this->eventListener.startListening();

	// The application has been initialized, so broadcast the event.
	this->eventBroadcaster->queueEvent(APP_INITIALIZED);

	return true;
}

void EventApplication::onUpdate(const nox::Duration& deltaTime)
{
	SdlApplication::onUpdate(deltaTime);

	this->tpsUpdateTimer.spendTime(deltaTime);

	if (this->tpsUpdateTimer.timerReached() == true)
	{
		// Broadcast that the TPS has been updated and what it is.
		auto tpsUpdateEvent = std::make_shared<TpsUpdated>(this->getTps());
		this->eventBroadcaster->queueEvent(tpsUpdateEvent);

		this->tpsUpdateTimer.reset();
	}
}

void EventApplication::onDestroy()
{
	/*
	 * The ListenerManager destructor will automatically stop listening, but because the event manager will be destroyed
	 * before this class, it will try to call a function on a garbage IBroadcaster pointer, causing a crash.
	 * Because of this we have to manually ensure that it stops listening.
	 *
	 * Usually you will listen for events only in the logic where this won't be a problem since logic subsystems
	 * are destroyed before the logic event manager.
	 */
	this->eventListener.stopListening();
}

/*
 * This is called when an event that we listen for is received.
 */
void EventApplication::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	if (event->isType(APP_INITIALIZED))
	{
		// Handle that the application was initialized.
		this->log.info().format("Received event that application was initialized (%s).", APP_INITIALIZED.c_str());
	}
	else if (event->isType(TpsUpdated::ID))
	{
		/*
		 * Handle that the TPS was updated.
		 * The event has to be cast to TpsUpdated so that we can get the proper data.
		 * It is very important that we do the event->isType(TpsUpdated::ID) check so that
		 * we can safely cast it.
		 */
		auto tpsEvent = static_cast<TpsUpdated*>(event.get());

		this->log.info().format("Received event that TPS was updated to %.2f (%s).", tpsEvent->getTps(), TpsUpdated::ID.c_str());
	}
}
