/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "TpsUpdated.h"

/*
 * The IdType is a std::string so we have to define it as this.
 * You should strive to make it a unique string, so here we use the application
 * name as a prefix to the event name.
 * The NOX Engine use the prefix "nox" and what subsystem it is located in.
 * For example "nox.event.broadcast_complete".
 */
const TpsUpdated::IdType TpsUpdated::ID = "08-event.tps_updated";

/*
 * We need to pass the event ID to the Event base class so that other users may query
 * what event it is.
 */
TpsUpdated::TpsUpdated(const float tps):
	nox::logic::event::Event(ID),
	tps(tps)
{
}

float TpsUpdated::getTps() const
{
	return this->tps;
}
