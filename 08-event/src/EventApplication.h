/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef EVENTAPPLICATION_H_
#define EVENTAPPLICATION_H_

#include <nox/nox.h>

/*
 * This example shows how to start listening to, broadcast and handle events.
 *
 * In this example the events are broadcasted from and received in the same class, but in a production scenario
 * events will be broadcasted from one subsystem to completely different subsystems that don't know about each other.
 * The only things they have in common are the event types and the event broadcaster.
 *
 * When running this example you should see "[info][EventApplication] Received event that application was initialized (08-event.app_initialized)."
 * which is output when receiving the APP_INITIALIZED event. Next you should see "Received event that TPS was updated to x (08-event.tps_updated).",
 * where x is the TPS, each 500ms.
 *
 * This class inherits from nox::logic::event::IListener so that it can receive events through onEvent().
 *
 * The example is based on 03-updating.
 *
 * See the EventApplication.cpp file for more.
 */
class EventApplication: public nox::app::SdlApplication, public nox::logic::event::IEventListener
{
public:
	EventApplication();

	bool onInit() override;
	void onUpdate(const nox::Duration& deltaTime) override;
	void onDestroy() override;

private:
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	nox::logic::Logic* initializeLogic();

	nox::app::log::Logger log;
	nox::util::Timer<nox::Duration> tpsUpdateTimer;

	// This will manage all the event types that we listen for.
	nox::logic::event::EventListenerManager eventListener;

	// Pointer to the event broadcaster that we use.
	nox::logic::event::IEventBroadcaster* eventBroadcaster;
};

#endif
