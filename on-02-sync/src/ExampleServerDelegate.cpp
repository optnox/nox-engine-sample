#include "ExampleServerDelegate.h"
#include <nox/logic/net/ClientStats.h>

void ExampleServerDelegate::onClientNetworkEvent(nox::ClientId raiser, const std::shared_ptr<nox::logic::event::Event>)
{
    // Let's not handle this today.
}

unsigned ExampleServerDelegate::getLobbySize() const
{
    return 4;
}

void ExampleServerDelegate::onClientJoined(const nox::logic::net::ClientStats* client)
{
    this->createSynchronizedActor("Suttung", client->getUserData().getClientId());
}

void ExampleServerDelegate::onClientLeft(const nox::logic::net::ClientStats* client)
{
    this->destroyAllSynchronizedActors(client->getUserData().getClientId());
}
