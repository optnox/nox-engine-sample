/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "SyncApplication.h"

#include <json/value.h>
#include <glm/gtx/string_cast.hpp>
#include <cassert>

SyncApplication::SyncApplication():
	nox::app::SdlApplication("on-02-sync", "SuttungDigital"),
	window(nullptr),
	eventListener(this->getName()),
    isClient(false),
    isServer(false),
	printRtt(false),
    cListenToBroadcast(false),
    cConnectToBroadcast(false),
    cConnect(false),
    cIsConnected(false),
    cNetMgr(nullptr),
    sBroadcast(false),
    sAutoBan(false),
    sBanDuration(0),
    sNetMgr(nullptr)
{
}

bool SyncApplication::initializeResourceCache()
{
	const auto cacheSizeMb = 512u;
	auto resourceCache = std::make_unique<nox::app::resource::LruCache>(cacheSizeMb);

	resourceCache->setLogger(this->createLogger());

	// We need to get resources from the project specific assets.
	const auto assetDir = this->getName() + "/assets";
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(assetDir)) == false)
	{
		this->log.error().format("Could not initialized resource cache to \"%s\".", assetDir.c_str());
		return false;
	}

	// We need to get resources from the common assets.
	const auto commonAssetsDirectory = std::string{"assets"};
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(commonAssetsDirectory)) == false)
	{
		this->log.error().format("Could not initialized resource cache to \"%s\".", commonAssetsDirectory.c_str());
		return false;
	}

	// We need to get resources from the NOX assets.
	const auto noxAssetsDirectory = std::string{"nox-engine/assets"};
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(noxAssetsDirectory)) == false)
	{
		this->log.error().format("Could not initialized resource cache to \"%s\".", noxAssetsDirectory.c_str());
		return false;
	}

	resourceCache->addLoader(std::make_unique<nox::app::resource::JsonLoader>(this->createLogger()));

	this->setResourceCache(std::move(resourceCache));

	return true;
}

nox::logic::Logic* SyncApplication::initializeLogic()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	auto logicPtr = logic.get();
	logicPtr->pause(false);

	this->addProcess(std::move(logic));

	return logicPtr;
}

nox::logic::physics::PhysicsSimulation* SyncApplication::initializePhysics(nox::logic::Logic* logic)
{
	auto physics = std::make_unique<nox::logic::physics::Box2DSimulation>(logic);
	physics->setLogger(this->createLogger());

	auto physicsPtr = physics.get();

	logic->setPhysics(std::move(physics));

	return physicsPtr;
}

nox::logic::world::WorldManager* SyncApplication::initializeWorldManager(nox::logic::Logic* logic)
{
	auto world = std::make_unique<nox::logic::world::WorldManager>(logic);

	world->registerActorComponent<nox::logic::actor::Transform>();
	world->registerActorComponent<nox::logic::physics::ActorPhysics>();
	world->registerActorComponent<nox::logic::graphics::ActorSprite>();
	world->registerActorComponent<nox::logic::graphics::ActorLight>();

	/*
	 * Register components to control Actors.
	 * These components will listen to control::Action events and respond to their data
	 * by either applying a force in the control direction or rotating in the control direction.
	 * The control::Action they receive is sent from ControllingWindowView. See that for more info.
	 */
	world->registerActorComponent<nox::logic::control::Actor2dDirectionControl>();
	world->registerActorComponent<nox::logic::control::Actor2dRotationControl>();

	const auto actorDirectory = std::string{"actor"};
	world->loadActorDefinitions(this->getResourceAccess(), actorDirectory);

	auto worldPtr = world.get();

	logic->setWorldManager(std::move(world));

	return worldPtr;
}

bool SyncApplication::loadWorldFile(nox::logic::ILogicContext* logicContext, nox::logic::world::WorldManager* worldManager)
{
	const auto worldFileDescriptor = nox::app::resource::Descriptor{"world/controllableWorld.json"};
	const auto worldFileHandle = this->getResourceAccess()->getHandle(worldFileDescriptor);

	if (worldFileHandle == nullptr)
	{
		this->log.error().format("Could not load world: %s", worldFileDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		const auto jsonData = worldFileHandle->getExtraData<nox::app::resource::JsonExtraData>();

		if (jsonData == nullptr)
		{
			this->log.error().format("Could not get JSON data for world: %s", worldFileDescriptor.getPath().c_str());
			return false;
		}
		else
		{
			auto loader = nox::logic::world::WorldLoader{logicContext};

			/*
			 * We register the window as a controlling View on index 0. This means that a View
			 * in the world JSON file can refer to this window by using index 0. See the controllableWorld.json file for more.
			 */
			loader.registerControllingView(0, this->window);

			if (loader.loadWorld(jsonData->getRootValue(), worldManager) == false)
			{
				this->log.error().format("Failed loading world \"%s\".", worldFileDescriptor.getPath().c_str());
				return false;
			}
		}
	}

	this->log.verbose().format("Loaded world \"%s\"", worldFileDescriptor.getPath().c_str());

	return true;
}

void SyncApplication::initializeWindow(nox::logic::Logic* logic)
{
	auto window = std::make_unique<ControllingWindowView>(this, this->getName());

	this->window = window.get();

	logic->addView(std::move(window));
}

void SyncApplication::initServer()
{
    auto snv = std::make_unique<nox::logic::net::ServerNetworkManager>(
            &this->packetTranslator, &this->serverDelegate);
    this->sNetMgr = snv.get();

    this->logic->setServerNetworkManager(std::move(snv));

    // Start a server on a potentially free port (this is required)
    if (!this->sNetMgr->startServer(8989))
    {
        this->log.fatal().raw("Unable to start server on port 8989");
    }

    if (this->sBroadcast)
    {
        // Enable broadcasting of our location
        this->sNetMgr->setEnableDiscoveryBroadcast(true);
    }
}

void SyncApplication::initClient()
{
    if (this->cUserData.getUserName() == "")
    {
        this->cUserData.setUserName("Unnamed Client");
    }

	// Setup our ListenerManager to manage this listening to this->eventBroadcaster.
	this->eventListener.addEventTypeToListenFor(nox::logic::net::ServerBroadcastEvent::ID);

    auto cnv = std::make_unique<nox::logic::net::ClientNetworkManager>(&this->packetTranslator);
    this->cNetMgr  = cnv.get();

    if (this->cListenToBroadcast)
    {
        this->cNetMgr->setBroadcastListening(true);
    }

    this->logic->setClientNetworkManager(std::move(cnv));

    if (this->cConnect)
    {
        connectToServer(this->cHostname, SERV_TCP_PORT);
    }
}

void SyncApplication::parseCmdArgs()
{
    // If the condition is false, print usage information and
    // exit with a bad status.
    #define FAIL_COND(_COND)   if ((_COND)){ printf("FAIL_COND: '" #_COND "'\n"); printUsage(); exit(-1);}

    // Parse the command line arguments. Exit with a bad
    // status upon misuse.
    auto args = this->getApplicationArguments();
    auto it = args.begin();

    // Ignore the startup command
    it++;

    while (it != args.end())
    {
        std::string arg = *it;

		if (arg == "-rtt")
		{
			this->printRtt = true;
		}
		else if (arg == "-c")
        {
            this->isClient= true;
        }
        else if (arg == "-s")
        {
            this->isServer = true;
        }
        else if (arg == "-b")
        {
            FAIL_COND(!this->isServer);
            this->sBroadcast = true;
        }
        else if (arg == "-a")
        {
            FAIL_COND(!this->isServer);
            this->sAutoBan = true;
        }
        else if (arg == "-d")
        {
            FAIL_COND(!this->isServer || !this->sAutoBan);
            it++;
            this->sBanDuration = atoi((*it).c_str());
            FAIL_COND(this->sBanDuration < 0);
        }
        else if (arg == "-con")
        {
            FAIL_COND(!this->isClient || this->cConnectToBroadcast);
            this->cConnect = true;

            it++;
            this->cHostname = *it;
        }
        else if (arg == "-l")
        {
            FAIL_COND(!this->isClient);
            this->cListenToBroadcast = true;
        }
        else if (arg == "-conb")
        {
            FAIL_COND(!this->isClient || this->cConnect);
            this->cListenToBroadcast = true;
            this->cConnectToBroadcast = true;
        }
        else if (arg == "-u")
        {
            FAIL_COND(!this->isClient);
            it++;
            this->cUserData.setUserName(*it);
        }
        else
        {
            FAIL_COND(false);
        }

        it++;
    }

    if (!this->isClient && !this->isServer)
    {
        printf("Must be either client or server.\n");
        printUsage();
        exit(-1);
    }

    #undef FAIL_COND
}

void SyncApplication::printUsage()
{
    printf("Usage: ./on-01-netsetup (-c [-con <host>|-l|-conb [-u <username>]]) \n");
    printf("                        (-s [-b][-a [-d <dur>]])[-rtt]\n");
	printf("    -rtt:	Print the round-trip-times (ping) once every second.\n");
    printf("    -c:     Run as a client\n");
    printf("    -con:   Connect to a host (requires -c)\n");
    printf("    -l:     Listen for broadcasts. Enabled with '-conb' as well (requires -c)\n");
    printf("    -conb:  Connect to the first broadcasting server to appear (requires -c)\n");
    printf("\n");
    printf("    -s:     Run as a server\n");
    printf("    -b:     Broadcast the server's existance to the local network (requires -s)\n");
    printf("    -a:     Automatically ban clients as they join >:) (requires -s)\n");
    printf("    -d:     The duration (in seconds) to ban joining clients for. The\n");
    printf("            duration must be a positive integer. (requires -s)\n");
}

bool SyncApplication::onInit()
{
	// Set the component channel to a measly 10hz. If this doesn't look good, the game will *never* look good.
	nox::logic::net::NetworkManager::setComponentChannelPacketInterval(nox::Duration(std::chrono::milliseconds(100)));

	if (this->SdlApplication::onInit() == false)
	{
		return false;
	}

	this->log = this->createLogger();
	this->log.setName("SyncApplication");

	if (this->initializeResourceCache() == false)
	{
		this->log.error().raw("Failed initializing resource cache.");
		return false;
	}

	this->logic = this->initializeLogic();
	auto eventBroadcaster = this->logic->getEventBroadcaster();
	this->initializePhysics(this->logic);
	auto worldManager = this->initializeWorldManager(this->logic);
	this->initializeWindow(this->logic);

	if (this->loadWorldFile(this->logic, worldManager) == false)
	{
		return false;
	}

    this->parseCmdArgs();

	this->eventListener.setup(this, eventBroadcaster);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ClientConnected::ID);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ClientDisconnected::ID);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ConnectionStarted::ID);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ConnectionFailed::ID);
    this->eventListener.addEventTypeToListenFor(nox::logic::net::ConnectionSuccess::ID);
    this->eventListener.addEventTypeToListenFor(nox::logic::world::ActorCreated::ID);
    this->eventListener.startListening();

    if (this->isServer)
    {
        this->initServer();
    }

    if (this->isClient)
    {
        this->initClient();
    }

	return true;
}

void SyncApplication::onDestroy()
{
    this->eventListener.stopListening();
}

void SyncApplication::connectToServer(std::string host, Uint16 port)
{
    this->log.debug().format("Attempting to connect to %s:%u...",
            host.c_str(), port);

    if (this->cIsConnected)
    {
        this->log.warning().raw("connectToServer called, but is already connected to a server");
        return;
    }

    if (!this->cNetMgr->connectToServer(host, port, this->cUserData))
    {
        this->log.error().format("Unable to connect to server %s:%u",
                host.c_str(), port);
        return;
    }

    this->log.debug().format("Connected to %s:%u", host.c_str(), port);

    this->cIsConnected = true;
}

void SyncApplication::printRoundTripTimes()
{
	// Retrieve either of the NetworkManagers
	nox::logic::net::NetworkManager *netMgr = nullptr;

	if (this->isServer)
	{
		netMgr = this->logic->getServerNetworkManager();
	}
	else if (this->isClient)
	{
		netMgr = this->logic->getClientNetworkManager();
	}
	else
	{
		return;
	}

	const std::vector<const nox::logic::net::ClientStats*>& clients = netMgr->getConnectedClients();
	if (clients.size() == 0)
	{
		return;
	}

	// Print using printf() instead of Logger to guarantee the contents is displayed
	// 		 14               2    8		  15
	printf("+----------------+----+----------+-----------------+\n");
	printf("| USER NAME      | ID | PING(ms) | Clock Diff (ms) |\n");
	printf("+----------------+----+----------+-----------------+\n");

	for (auto clientStats : clients)
	{
		const char *name = clientStats->getUserData().getUserName().c_str();
		const nox::ClientId clientId = clientStats->getUserData().getClientId();
		const nox::Duration dur = clientStats->getRoundTripTime();
		const unsigned ms = std::chrono::duration_cast<std::chrono::milliseconds>(dur).count();
		const long int clockDiff = clientStats->getClockDifference();

		printf("| %14s | %2u | %8u | %15ld |\n", name, clientId, ms, clockDiff);
	}

	printf("+----------------+----+----------+-----------------+\n");
}


void SyncApplication::onUpdate(const nox::Duration& deltaTime)
{
	this->SdlApplication::onUpdate(deltaTime);

	assert(this->window != nullptr);
	this->window->render();

	if (this->printRtt && this->rttClock.getElapsedTime() > nox::Duration(std::chrono::seconds(1)))
	{
		this->printRoundTripTimes();
		this->rttClock.start();
	}
}

void SyncApplication::onSdlEvent(const SDL_Event& event)
{
	this->SdlApplication::onSdlEvent(event);

	assert(this->window != nullptr);
	this->window->onSdlEvent(event);
}

void SyncApplication::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	if (event->isType(nox::logic::net::ServerBroadcastEvent::ID))
	{
        auto bc = (nox::logic::net::ServerBroadcastEvent*)event.get();

        nox::logic::net::ServerConnectionInfo info = bc->getServerConnectionInfo();

        this->log.info().format("Server %sappeared:", (bc->getOnline() ? "" : "dis"));
        this->log.info().format("%s   %s (%s)   %u/%u    %s:%u\n",
                info.serverName.c_str(),
                info.appId.c_str(),
                info.versionId.c_str(),
                info.connectedClients,
                info.maxClients,
                info.hostname.c_str(),
                info.tcpPort);

        if (!this->cIsConnected && this->cConnectToBroadcast)
        {
            this->connectToServer(info.hostname, info.tcpPort);
        }
	}
    else if (event->isType(nox::logic::net::ClientConnected::ID))
    {
        auto con = (nox::logic::net::ClientConnected*)event.get();
        this->log.info().format("Client %s (id = %u, hash = %s) connected",
                con->getUserData().getUserName().c_str(),
                con->getUserData().getClientId(),
				con->getUserData().getUserHash().c_str());

        if (this->isServer && this->sAutoBan)
        {
            nox::ClientId id = con->getUserData().getClientId();
            nox::Duration duration(std::chrono::seconds(this->sBanDuration));

            this->log.debug().format("Banning client %s (%u) for %u seconds %s",
                    con->getUserData().getUserName().c_str(),
                    id,
                    this->sBanDuration,
                    (this->sBanDuration ? "" : "(indefinitely)"));
            this->sNetMgr->banClient(id, duration, "Automatic ban. No hard feelings.");
        }
    }
    else if (event->isType(nox::logic::net::ClientDisconnected::ID))
    {
        auto dc = (nox::logic::net::ClientDisconnected*)event.get();
        this->log.info().format("Client %u disconnected", dc->getClientId());
    }
    else if (event->isType(nox::logic::net::ConnectionStarted::ID))
    {
        auto init = (nox::logic::net::ConnectionStarted*)event.get();
        this->log.info().format("Connection started: %s",
                init->getServerHostname().c_str());
    }
    else if (event->isType(nox::logic::net::ConnectionFailed::ID))
    {
        auto fail = (nox::logic::net::ConnectionFailed*)event.get();
        this->log.info().format("Connection to server '%s' failed",
                fail->getServerHostname().c_str());
    }
    else if (event->isType(nox::logic::net::ConnectionSuccess::ID))
    {
        auto success = (nox::logic::net::ConnectionSuccess*)event.get();
        this->log.info().format("Connected successfully to server %s as %s (ClientId %u)",
                success->getServerHostname().c_str(),
                success->getUserData().getUserName().c_str(),
                success->getUserData().getClientId());
    }
    else if (event->isType(nox::logic::world::ActorCreated::ID))
    {
        auto created = (nox::logic::world::ActorCreated*)event.get();
        nox::logic::actor::Actor* actor = created->getActor();

        if (actor->isOwnedLocally())
        {
            this->window->setControlledActor(actor);
        }
    }
}
