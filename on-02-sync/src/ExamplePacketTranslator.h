#ifndef EXAMPLEPACKETTRANSLATOR_H_
#define EXAMPLEPACKETTRANSLATOR_H_

#include <nox/nox.h>


class ExamplePacketTranslator: public nox::logic::net::IPacketTranslator
{
    std::shared_ptr<nox::logic::event::Event> translatePacket(std::string eventId, const nox::logic::net::Packet*);
};

#endif
