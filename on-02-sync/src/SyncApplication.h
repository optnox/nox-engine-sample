/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef CONTROLAPPLICATION_H_
#define CONTROLAPPLICATION_H_

#include "ControllingWindowView.h"
#include "ExamplePacketTranslator.h"
#include "ExampleServerDelegate.h"

#include <nox/nox.h>

const Uint16 SERV_TCP_PORT = 8989;

class DummyPacketTranslator { };
class DummyServerDelegate { };

/*
 * This is a *nasty ass* merged Application based on 11-control and on-01-netsetup.
 *
 * It attempts to demonstrate how to implement networking... To some extent it does what
 * it aims to do, but god damn, it could use some prettyfying.
 */
class SyncApplication: public nox::app::SdlApplication, public nox::logic::event::IEventListener
{
public:
	SyncApplication();

	bool onInit() override;
    void onDestroy() override;
	void onUpdate(const nox::Duration& deltaTime) override;
	void onSdlEvent(const SDL_Event& event) override;

private:
	bool initializeResourceCache();
	nox::logic::Logic* initializeLogic();
	nox::logic::physics::PhysicsSimulation* initializePhysics(nox::logic::Logic* logic);
	nox::logic::world::WorldManager* initializeWorldManager(nox::logic::Logic* logic);

    void initServer();
    void initClient();
    void parseCmdArgs();
    void printUsage();
    void connectToServer(std::string host, Uint16 port);
	void printRoundTripTimes();

	void initializeWindow(nox::logic::Logic* logic);
	bool loadWorldFile(nox::logic::ILogicContext* context, nox::logic::world::WorldManager* worldManager);

    void onEvent(const std::shared_ptr<nox::logic::event::Event>& event);

	nox::app::log::Logger log;
	ControllingWindowView* window;

    nox::logic::Logic* logic;
    ExamplePacketTranslator packetTranslator;
    ExampleServerDelegate serverDelegate;

	nox::logic::event::EventListenerManager eventListener;

    bool isClient;
    bool isServer;

	nox::util::Clock rttClock;
	bool printRtt;

    // Client options (prefixed with "c") - real world applications should use a
    // less cluttered approach.
    bool cListenToBroadcast;
    bool cConnectToBroadcast;
    bool cConnect;
    std::string cHostname;
    bool cIsConnected;
    nox::logic::net::UserData cUserData;
    nox::logic::net::ClientNetworkManager *cNetMgr;

    // Server options (prefixed with "s")
    bool sBroadcast;
    bool sAutoBan;
    unsigned sBanDuration;
    nox::logic::net::ServerNetworkManager *sNetMgr;
};

#endif
