#ifndef EXAMPLESERVERDELEGATE_H_
#define EXAMPLESERVERDELEGATE_H_

#include <nox/nox.h>

class ExampleServerDelegate: public nox::logic::net::ServerDelegate
{
public:
    void onClientNetworkEvent(nox::ClientId raiser, const std::shared_ptr<nox::logic::event::Event>) override;

    unsigned getLobbySize() const;

    void onClientJoined(const nox::logic::net::ClientStats*);

    void onClientLeft(const nox::logic::net::ClientStats*);
};

#endif
