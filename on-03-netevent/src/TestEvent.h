#ifndef TESTEVENT_H_
#define TESTEVENT_H_

#include <nox/nox.h>

class TestEvent: public nox::logic::event::Event
{
public:
	static const IdType ID;

	TestEvent(const int content);
    TestEvent(const nox::logic::net::Packet* packet);

	void doSerialize() const override;
	void doDeSerialize() override;

	int getContent();
private:
	int content;
};
#endif
