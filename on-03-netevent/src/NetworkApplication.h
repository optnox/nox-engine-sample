/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NETWORKAPPLICATION_H_
#define NETWORKAPPLICATION_H_

#include <nox/nox.h>
#include "TestEvent.h"


const Uint16 SERV_TCP_PORT = 8989;

class DummyPacketTranslator: public nox::logic::net::IPacketTranslator
{
public:
	std::shared_ptr<nox::logic::event::Event>
		translatePacket(std::string eventId, const nox::logic::net::Packet *packet) override
	{
		if (eventId == TestEvent::ID)
		{
			return std::make_shared<TestEvent>(packet);
		}

		return nullptr;
	}
};

class DummyServerDelegate: public nox::logic::net::ServerDelegate
{
public:
	void onClientNetworkEvent(nox::ClientId raiser, const std::shared_ptr<nox::logic::event::Event> event) override
	{
	}

	unsigned getLobbySize() const override
	{
		return 11;
	}

	virtual void onClientJoined(const nox::logic::net::ClientStats *client) override
	{

	}

	virtual void onClientLeft(const nox::logic::net::ClientStats *client) override
	{

	}
};




class NetworkApplication: public nox::app::SdlApplication, public nox::logic::event::IEventListener
{
public:
	NetworkApplication();

	bool onInit() override;
	void onUpdate(const nox::Duration& deltaTime) override;
	void onDestroy() override;

private:
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	void initServer();

	void initClient();
	void connectToServer(std::string hostname, Uint16 tcpPort);

	void parseCmdArgs();
	void printUsage();

	nox::logic::Logic* initializeLogic();


	nox::logic::Logic* logic;
	nox::app::log::Logger log;
	DummyPacketTranslator packetTranslator;
	DummyServerDelegate serverDelegate;

	nox::logic::event::EventListenerManager eventListener;
	nox::logic::event::IEventBroadcaster* eventBroadcaster;

	bool isClient;
	bool isServer;

	// Client options (prefixed with "c") - real world applications should use a
	// less cluttered approach.
	bool cConnectToBroadcast;
	bool cConnect;
	std::string cHostname;
	bool cIsConnected;
	nox::logic::net::UserData cUserData;
	nox::logic::net::ClientNetworkManager *cNetMgr;

	// Server options (prefixed with "s")
	bool sBroadcast;
	bool sAutoBan;
	unsigned sBanDuration;
	nox::logic::net::ServerNetworkManager *sNetMgr;
	int sMessagesSent;
};

#endif
