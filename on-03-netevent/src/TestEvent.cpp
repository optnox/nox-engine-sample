#include "TestEvent.h"
#include <iostream>

const nox::logic::event::Event::IdType TestEvent::ID = "on-02-netevent.test_event";

TestEvent::TestEvent(const int content):
	nox::logic::event::Event(ID),
	content(content)
{
}

TestEvent::TestEvent(const nox::logic::net::Packet* packet):
    Event(ID)
{
	this->deSerialize(packet);
}

void TestEvent::doSerialize() const
{
	nox::logic::event::Event::doSerialize();
	*this << this->content;
}

void TestEvent::doDeSerialize()
{
	nox::logic::event::Event::doDeSerialize();
	*this >> this->content;
}

int TestEvent::getContent()
{
	return this->content;
}
